app.service('ScopeGlobal', function () {
    var item = {};
    var token = '';
    return {
        getItem: function () {
            return item;
        }
        , setItem: function (value) {
            item = value;
        },
        
        getToken: function(){
            return token;
        },
        setToken: function(value){
            token = value;
        }
    };
});