var db = null;

var app = angular.module('starter', ['ionic', 'ui.utils.masks', 'ngCookies', 'ngCordova']);

app.run(function($ionicPlatform, $ionicHistory, $state, $rootScope, $cordovaSQLite) {

    $ionicPlatform.registerBackButtonAction(function(event) {
        if ($state.current.name == "app.dashboard" || $state.current.name == "login") {
            ionic.Platform.exitApp();
        } else {
            $ionicHistory.goBack()
        }
    }, 100);

    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }

        //olhar video abaixo para concluir a utilização do ADMOB
        //https://www.youtube.com/watch?t=45&v=PmUnan_oXmQ
        //https://blog.nraboy.com/2014/06/using-admob-ionicframework/

        if (window.plugins && window.plugins.AdMob) {
            var admob_key = device.platform == "Android" ? "ANDROID_PUBLISHER_KEY" : "IOS_PUBLISHER_KEY";
            var admob = window.plugins.AdMob;
            admob.createBannerView({
                    'publisherId': admob_key,
                    'adSize': admob.AD_SIZE.BANNER,
                    'bannerAtTop': false
                },
                function() {
                    admob.requestAd({ 'isTesting': false },
                        function() {
                            admob.showAd(true);
                        },
                        function() { console.log('failed to request ad'); }
                    );
                },
                function() { console.log('failed to create banner view'); }
            );
        }

        //Inicialização do sqlite
        db = $cordovaSQLite.openDB("my.db");
        $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS users (login varchar(100), pw varchar(100))");

    });


});

app.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('login', {
        url: "/login",
        templateUrl: "templates/login.html",
        controller: "LoginCtrl"
    })

    .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'MenuCtrl'
    })

    .state('app.dashboard', {
        url: "/dashboard",
        views: {
            'menuContent': {
                templateUrl: "templates/dashboard.html",
                controller: "DashboardCtrl"
            }
        }
    })

    .state('app.dashboard/detalhes', {
        url: "/dashboard/detalhes",
        views: {
            'menuContent': {
                templateUrl: "templates/dashboard-detalhes.html",
                controller: "DashboardDetalhesCtrl"
            }
        }
    })

    .state('app.relatorio', {
        url: "/relatorio",
        views: {
            'menuContent': {
                templateUrl: "templates/relatorio.html",
                controller: "RelatorioCtrl"
            }
        }
    })

    .state('app.relatorio/detalhes', {
        url: "/relatorio/detalhes",
        views: {
            'menuContent': {
                templateUrl: "templates/relatorio-detalhes.html",
                controller: "RelatorioDetalhesCtrl"
            }
        }
    });

    $urlRouterProvider.otherwise('login');
});
