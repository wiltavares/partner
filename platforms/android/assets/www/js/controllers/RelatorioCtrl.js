app.controller('RelatorioCtrl', ["$scope", "$rootScope", "$ionicPopup", "$state", "ScopeGlobal",
    function ($scope, $rootScope, $ionicPopup, $state, ScopeGlobal) {

    $scope.item = {};
    $scope.param = {};
    $rootScope.init();
                
    $scope.buscar = function(){

        if ($scope.item.dtInit != undefined && $scope.item.dtFinal != undefined)
        {   
            $scope.param.dtInit = moment($scope.item.dtInit).format(); 
            $scope.param.dtFinal = moment($scope.item.dtFinal).format();
            ScopeGlobal.setItem($scope.param);
            $state.go('app.relatorio/detalhes');
        }        
        else
        {
            $rootScope.showAlert('Preencha todos os campos!', 'Atenção');
        }
    }
    
}]);