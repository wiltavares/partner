app.controller('LoginCtrl', ["$scope", "$rootScope", "$ionicPopup", "$state", "$cookies", "$http", "ScopeGlobal", "WebServices", "sqliteService",
    function($scope, $rootScope, $ionicPopup, $state, $cookies, $http, ScopeGlobal, WebServices, sqliteService) {
        $scope.item = {};

        $scope.loading = false;
        
        $scope.logar = function(item) {
            $scope.loading = true;
            $scope.user = angular.copy(item);

            WebServices.login($scope.user).success(function (response) { 
                
                if (response.fieldErrors.length > 0)
                {
                    var mensagem = response.fieldErrors[0].status == "InvalidUsernameOrPassword" ? 'Login e/ou senha inválido(s)' : 'Por favor tente novamente!';
                    
                    $rootScope.showAlert(mensagem, 'Erro ao logar');
                    $scope.loading = false;
                }
                else
                {
                    $scope.user.data = $rootScope.montaJson(response.data);
                    ScopeGlobal.setToken($scope.user.data.Token);
                    $cookies.putObject('usuario', $scope.user);
                    // sqliteService.insert($scope.user.login, $scope.user.pw);
                    $scope.loading = false;
                    $state.go('app.dashboard');
                }

            }).error(function(error){
                $rootScope.showAlert(error);
                $scope.loading = false;
            });
        }
    }
]);