app.controller('DashboardCtrl', ["$scope", "$rootScope", "$ionicPopup", "$state", "ScopeGlobal", "sqliteService", "WebServices", "DeParaAlert",
    function($scope, $rootScope, $ionicPopup, $state, ScopeGlobal, sqliteService, WebServices, DeParaAlert) {

        //$scope.usuario = sqliteService.select("user");
        $scope.ordem = {};
        $scope.item = {};
        $scope.loading = false;
        $scope.ordem.numero = '';
        

        $scope.detalhar = function() {
            ScopeGlobal.setItem($scope.item);
            $state.go('app.dashboard/detalhes');
        }

        $scope.limpar = function() {
            $scope.ordem.numero = '';
        }

        $scope.validateOrder = function(){
            $scope.item = {};
            $scope.loading = true;
            WebServices.validateOrder($scope.ordem.numero).success(function(response){
                if (response.fieldErrors.length > 0)
                {
                    $rootScope.showAlert('o código informado não está disponível.<br> status: '+DeParaAlert[response.fieldErrors[0].status]+'', 'Erro na validação');
                    $scope.ordem.numero = '';
                    $scope.loading = false;
                }
                else
                {
                    $scope.buscarOrdem();
                }
            }).error(function(error){
                $rootScope.showAlert(DeParaAlert[Default]);
                $scope.loading = false;
            })   
        }

        $scope.buscarOrdem = function(){
            $scope.loading = true;   

            WebServices.getOrder($scope.ordem.numero).success(function(response){
                $scope.item = response;
                $scope.item.orderInformado = $scope.ordem.numero;
                $scope.loading = false;
            }).error(function(error){
                $rootScope.showAlert(DeParaAlert[Default]);
                 $scope.loading = false;
             })
              
        }

        $scope.init = function(){
            
            $rootScope.init();
            $scope.limpar();
        }
        $scope.init();

    }
]);