app.controller('RelatorioDetalhesCtrl', ["$scope", "$rootScope", "$ionicPopup", "$ionicModal", "$state", "ScopeGlobal", "WebServices",
    function ($scope, $rootScope, $ionicPopup, $ionicModal, $state, ScopeGlobal, WebServices) {
    
    $rootScope.init();
    $scope.item = {};
    $scope.param = {};
    $scope.loading = false;

    $scope.init = function(){
        //$scope.loading = true;
        $scope.param = ScopeGlobal.getItem();

        if ($scope.param.dtInit != undefined && $scope.param.dtFinal != undefined)
        {
            
            WebServices.reportPartner($scope.param).success(function(response){
                $scope.item = response;
                $scope.loading = false;
            }).error(function(error){
                $rootScope.showAlert(error);
                $scope.loading = false;
            });
        }
    }                                         
    $scope.init();
    
}]);