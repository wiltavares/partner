app.factory('WebServices', ['$http', '$cookies', 'ScopeGlobal', function ($http, $cookies, ScopeGlobal) {    
    var url = 'http://178.18.123.116:8013/WAPartnerRest';
    
    return {    
         
        login: function (user) {    
            return $http({    
                crossDomain: true,
                async: true,
                method: 'post',    
                headers: {    
                     'Content-Type': 'application/json' 
                },    
                //url: url +'/login',
                url: url+'/login',    
                data: {
                    // "Username" : "u1@gmail.com",
                    // "Password" : "WAlondon1#"

                    "Username" : user.Username,
                    "Password" : user.Password
                }                
            });  
        },  
        
        getOrder: function (orderCode) {    
            
            return $http({ 
                crossDomain: true,
                async: true,   
                method: 'get',    
                headers: {    
                     'Content-Type': 'application/json; charset=utf-8',
                     'Token' : ScopeGlobal.getToken()  
                },    
                url: url +'/order/'+orderCode+''
            });    
        },

        validateOrder: function (orderCode){
            return $http({ 
                crossDomain: true,
                async: true,   
                method: 'get',    
                headers: {    
                     'Content-Type': 'application/json; charset=utf-8',
                     'Token' : ScopeGlobal.getToken()  
                },    
                url: url +'/order/validate/'+orderCode+''
            });
        },

        rejectedOrer: function(orderCode, cause){
            debugger;
            return $http({
                crossDomain: true,
                async: true,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                     'Token' : ScopeGlobal.getToken()
                },
                url: url +'/order/rejected',
                data: {
                    "OrderCode": orderCode,
                    "Cause": cause
                }
            })
        },

        acceptedOrder: function(order){
            return $http({
                crossDomain: true,
                async: true,
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                     'Token' : ScopeGlobal.getToken()
                },
                url: url +'/order/accepted',
                data: {
                    "OrderCode": order.orderInformado,
                    "ReceivedValue": order.contabilizado,
                    "CompanyId": order.loja.Id,
                    "IsManual": order.calcularAutomaticamente,
                    "Password": $cookies.getObject('usuario').Password,
                    "PassPhrase": null
                }
            })    
        },

        getCompany: function(){
            return $http({ 
                crossDomain: true,
                async: true,   
                method: 'get',    
                headers: {    
                     'Content-Type': 'application/json; charset=utf-8',
                     'Token' : ScopeGlobal.getToken()  
                },    
                url: url +'/company/byTenantAndPartner'
            });     
        },

        reportPartner: function(item){
            return $http({ 
                crossDomain: true,
                async: true,   
                method: 'post',    
                headers: {    
                     'Content-Type': 'application/json; charset=utf-8',
                     'Token' : ScopeGlobal.getToken()  
                },    
                url: url +'/reportPartner?dtInit='+item.dtInit+'&dtFinal='+item.dtFinal+''
            });    
        }
    };    
}]);