app.service('DeParaAlert', function () {
    
    return {
        Default: 'Ocorreu um erro durante o processamento, por favor tente novamente',
        BranchIncorrect: 'Não é possível aprovar esta ordem através do aplicativo',
        OrderCodeNotFound: 'Ordem não econtrada',
        InvalidOrderState: 'Estado da ordem inválido',
        ExpiredOrder: 'Ordem expirada'
    };
});