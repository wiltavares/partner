app.controller('IndexCtrl', ["$scope", "$rootScope", "$ionicPopup", "$state", "$cookies", "ScopeGlobal", 
    function ($scope, $rootScope, $ionicPopup, $state, $cookies, ScopeGlobal) {
    
    $rootScope.deParaAlert = {
        Default: 'Ocorreu um erro durante o processamento, por favor tente novamente',
        BranchIncorrect: 'Não é possível aprovar esta ordem através do aplicativo',
        OrderCodeNotFound: 'Ordem não econtrada',
        InvalidOrderState: 'Estado da ordem inválido',
        ExpiredOrder: 'Ordem expirada'
    }

    $rootScope.init = function(){
        
        $scope.user = $cookies.getObject('usuario');
        
        if ($scope.user == undefined)
        {
            $state.go('login');
        }
        else
        {
            if (ScopeGlobal.getToken() == '')
            {
                ScopeGlobal.setToken($scope.user.data.Token);
            }
        }
    }

    $rootScope.montaJson = function(objeto){
        var json = {};
        objeto.forEach(function(obj) {
            obj = angular.fromJson(obj);

            json[Object.keys(obj)[0]] = obj[Object.keys(obj)[0]];
        }); 
        return json;
    }

    $rootScope.showAlert = function(mensagem, titulo){

        var alertPopup = $ionicPopup.alert({
            title: titulo || 'Erro' ,
            template: '<span>'+mensagem+'</span>'
        });

        alertPopup.then(function(res) {

        });
    }
    
}]);
