app.controller('MenuCtrl', ["$scope", "$rootScope", "$state", "$cookies", function($scope, $rootScope, $state, $cookies) {

    $scope.data = new Date();

    $scope.menu = [
        { titulo: "Dashboard", endereco: "#/app/dashboard", icon: "ion-ios-home", ativo: true }, 
        { titulo: "Relatórios", endereco: "#/app/relatorio", icon: "ion-ios-pulse-strong", ativo: false }
    ];

    $scope.logoff = function() {

        $cookies.putObject('usuario', undefined);
        $state.go('login');
    }
}]);