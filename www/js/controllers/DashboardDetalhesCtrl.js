app.controller('DashboardDetalhesCtrl', ["$scope", "$rootScope", "$ionicPopup", "$ionicModal", "$state", "ScopeGlobal", "WebServices", "DeParaAlert", 
    function ($scope, $rootScope, $ionicPopup, $ionicModal, $state, ScopeGlobal, WebServices, DeParaAlert) {
    
    $rootScope.init();
    $scope.item = {};  
    $scope.lojas = [];
    $scope.notas = {"cinquenta": 0, "vinte": 0, "dez": 0, "cinco": 0, "dois": 0, "um": 0};
    $scope.moedas = {"cinquenta": 0, "vinte": 0, "dez": 0, "cinco": 0, "dois": 0, "um": 0};    

    $scope.populaLojas = function(){
        WebServices.getCompany().success(function (response) {
            $scope.lojas = response;
        }).error(function(error){
            $rootScope.showAlert(DeParaAlert[Default]);
        });        
    }

    $scope.loadModal = function(){
        $ionicModal.fromTemplateUrl('templates/modais/notas.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modalNotas = modal;
        });

        $ionicModal.fromTemplateUrl('templates/modais/moedas.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modalMoedas = modal;
        });
    }
          
    $scope.openModalNotas = function() {
        $scope.modalNotas.show();
    };

    $scope.closeModalNotas = function() {
        $scope.item.totalNotas = ($scope.item.notas.cinquenta * 50) + ($scope.item.notas.vinte * 20) 
                    + ($scope.item.notas.dez * 10) + ($scope.item.notas.cinco * 5)
                    + ($scope.item.notas.dois * 2) + ($scope.item.notas.um * 1);

        $scope.totalizaContabilizado();
        $scope.modalNotas.hide();
    };
                    
    $scope.openModalMoedas = function() {
        $scope.modalMoedas.show();
    };

    $scope.closeModalMoedas = function() {

        $scope.item.totalMoedas = ($scope.item.moedas.cinquenta * 0.50) + ($scope.item.moedas.vinte * 0.20) 
                    + ($scope.item.moedas.dez * 0.10) + ($scope.item.moedas.cinco * 0.05)
                    + ($scope.item.moedas.dois * 0.02) + ($scope.item.moedas.um * 0.01);

        $scope.totalizaContabilizado();
        $scope.modalMoedas.hide();
    };

    $scope.totalizaContabilizado = function(){
        $scope.item.contabilizado = $scope.item.totalNotas + $scope.item.totalMoedas;
    }

    $scope.changeAutomatico = function(){
        $scope.item.notas = $scope.notas;
        $scope.item.moedas = $scope.moedas;
        $scope.item.totalNotas = 0;
        $scope.item.totalMoedas = 0;
        $scope.item.contabilizado = 0;
    }

    $scope.openPopupRejected = function(){
        $ionicPopup.confirm({
            title: 'Rejeitar Ordem',
            templateUrl: 'templates/popups/justificationRejectedOrder.html',
            scope: $scope,
            buttons: [
                { 
                    text: 'Fechar',
                    type: 'button-dark',
                    onTap: function(e){
                        $scope.item.cause = undefined;
                    }
                },
                {
                    text: 'Rejeitar',
                    type: 'button-assertive',
                    onTap: function(e) {
                        if (!$scope.item.cause) {
                            e.preventDefault();
                        } else {
                            return $scope.item;
                        }
                    }
                }
            ]
        }).then(function(res) {
            if (res != undefined)
                $scope.rejectedOrer();
        });
    }

    $scope.rejectedOrer = function(){
        
        WebServices.rejectedOrer($scope.item.orderInformado, $scope.item.cause).success(function (response) {  
             
            if (response)
            {
                $rootScope.showAlert('Ordem Rejeitada!', 'Notificação');
                $scope.item = {};
                $state.go('app.dashboard');
            }
            
        }).error(function(error){
            $rootScope.showAlert(DeParaAlert[Default]);
        });    
    }

    $scope.acceptedOrder = function(){
        if ($scope.item.contabilizado == $scope.item.AmmountDue && $scope.item.loja != undefined)
        {
            WebServices.acceptedOrder($scope.item).success(function (response) {  
                debugger;  
                if (response.fieldErrors.length > 0)
                {
                    $rootScope.showAlert('Ocorreu um erro durante o recebimento da ordem. <br>Status: '+DeParaAlert[response.fieldErrors[0].status]);    
                }
                else
                {
                    $rootScope.showAlert('Ordem Aceita!', 'Notificação');
                    $scope.item = {};
                    $state.go('app.dashboard');
                }
                
            }).error(function(error){
                $rootScope.showAlert(DeParaAlert[Default]);
            }); 
        }
        else
        {
            var mensagem = $scope.item.loja != undefined ? 'Valor contabilizado não corresponde ao valor total' : 'Preencha os campos corretamente';
            $rootScope.showAlert(mensagem, 'Atenção');
        }
    }

    $scope.init = function(){
        
        $scope.item = ScopeGlobal.getItem();
        $scope.item.calcularAutomaticamente = true;
        $scope.item.notas = $scope.notas;
        $scope.item.moedas = $scope.moedas;
        $scope.item.totalNotas = 0;
        $scope.item.totalMoedas = 0;
        $scope.item.contabilizado = 0;

        $scope.loadModal();
        $scope.populaLojas();
    }
    $scope.init();


    $scope.$watch("item.contabilizado", function (newValue) {
        if (newValue > $scope.item.AmmountDue)
            $rootScope.showAlert('Valor contabilizado superior ao valor total', 'Atenção');
    }, true);
}]);